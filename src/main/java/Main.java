package main.java;

public class Main {

    public static void main(String[] args) {
        Input input = new Input();
        long[] numbers = input.getNumbers();
        Luhn luhn = new Luhn(numbers);
        System.out.println();
        System.out.println("Input: " + numbers[0] + " " + numbers[1]);
        System.out.println("Provided: " + numbers[1]);
        System.out.println("Expected: " + luhn.getCheck());
        System.out.println();
        if (luhn.validate()) {
            System.out.println("Checksum: Valid");
        } else {
            System.out.println("Checksum: Invalid");
        }
        if (luhn.getLength() == 16) {
            System.out.println("Digits: 16 (credit card)");
        } else {
            System.out.println("Digits: " + luhn.getLength());
        }
    }
}
